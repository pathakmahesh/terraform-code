# Configure the AWS Provider
provider "aws" {
  region = var.region
  
  }

# keep the state file in s3 bucket
terraform {
  backend "s3" {
    
    bucket = "terraform-plan-file"
    key    = "terraformstatefile/vpc"
    region = "us-east-1"
  }
}

# Create a VPC
resource "aws_vpc" "test_vpc" {
  cidr_block = var.cidr_block

  tags = {
    Name  = "VPC_created_by_TF"
    Owner = "Devops"
  }
}